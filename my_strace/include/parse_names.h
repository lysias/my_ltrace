#ifndef PARSE_NAMES_H
# define PARSE_NAMES_H

/**
** @brief Parses /usr/include/asm/unistd_64.h to get the syscall names
**
** @return An array containing all the syscalls at the index that corresponds
** to their numerical value. For example, "close" will be at index 3 in the array
*/
char** get_syscall_names();
void free_tab(char** tab);

#endif /* !PARSE_NAMES_H */
