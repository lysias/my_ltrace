#ifndef PRINTER_H
# define PRINTER_H

# define PRINT_INT(prefix, reg) \
    fprintf(stderr, "%s = %d", prefix, (int)reg);

# define PRINT_SIZE_T(prefix, reg) \
    fprintf(stderr, "%s = %zd", prefix, (size_t)reg);

# define PRINT_POINTER(prefix, reg) \
    reg ? fprintf(stderr, "%s = %p", prefix, (void*)reg): fprintf(stderr, "NULL");

/**
** @brief Prints the syscall name as well as its arguments
**
** @param syscalls The list of all the syscalls
** @param pid The pid of the tracee
*/
void print_syscall(char** syscalls, struct user_regs_struct uregs, int pid);

#endif /* !PRINTER_H */

