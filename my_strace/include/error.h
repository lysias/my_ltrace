#ifndef ERROR_H
# define ERROR_H

# define ERROR(message) {                                  \
    printf("%s::%s(): %s\n", __FILE__, __func__, message); \
    exit(1);                                               \
}

#endif /* !ERROR_H */

