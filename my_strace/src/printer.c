#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include <asm/unistd_64.h>

#include "printer.h"

static char* get_string(int pid, unsigned long reg) {
    // strace only prints the first 32 bytes by default, but we need at most 35
    // for the "..."
    char* val = calloc(36, sizeof(char));
    long read = 0;
    long tmp;

    while (read < 32) {
        tmp = ptrace(PTRACE_PEEKDATA, pid, reg + read);
        memcpy(val + read, &tmp, sizeof(tmp));

        // If we encounter a \0, break
        if (memchr(&tmp, '\0', sizeof(tmp)) != NULL)
            break;

        read += sizeof(tmp);
    }
    // If we already read 32 characters but did not encounter a \0...
    if (read == 32 && memchr(&tmp, '\0', read))
        memcpy(val + read, "...", 3);

    return val;
}

static void print_string(char* prefix, int pid, unsigned long reg) {
    char* out = get_string(pid, reg);
    fprintf(stderr, "%s = \"%s\"", prefix, out);
    free(out);
}

static void print_struct(char** prefixes, int pid, unsigned long reg) {
    long tmp;
    int printed = 0;

    fprintf(stderr, ", {");
    for (int i = 0; prefixes[i]; ++i) {
        tmp = ptrace(PTRACE_PEEKDATA, pid, reg + i * sizeof(tmp));
        printed += fprintf(stderr, "%s = %lu", prefixes[i], tmp);

        // If more than 30 characters have been printed, break
        if (printed > 30) {
            fprintf(stderr, "...");
            break;
        }
    }
    fprintf(stderr, " }");
}

void print_syscall(char** syscalls, struct user_regs_struct uregs, int pid) {
    int syscall = uregs.orig_rax;
    fprintf(stderr, "%s(", syscalls[syscall]);

    switch (syscall)
    {
        case __NR_open:
            print_string("pathname", pid, uregs.rdi);
            PRINT_INT(", flags", uregs.rsi);
            break;
        case __NR_close:
            PRINT_INT("fd", uregs.rdi);
            break;
        case __NR_write:
            PRINT_INT("fd", uregs.rdi);
            print_string(", buf", pid, uregs.rsi);
            break;
        case __NR_mmap:
            PRINT_POINTER("addr", uregs.rdi);
            PRINT_SIZE_T(", length", uregs.rsi);
            PRINT_INT(", prot", uregs.rdx);
            PRINT_INT(", flags", uregs.r10);
            PRINT_INT(", fd", uregs.r8);
            PRINT_INT(", offset", uregs.r9);
            break;
        case __NR_munmap:
            PRINT_POINTER("addr", uregs.rdi);
            PRINT_SIZE_T(", length", uregs.rsi);
            break;
        case __NR_fstat:
            PRINT_INT("fd", uregs.rdi);
            char* prefixes[] = { "st_dev", ", st_ino", ", st_mode",
                                 ", st_nlink", ", st_uid", ", st_gid",
                                 ", st_rdev", ", st_size", ", st_blksize",
                                 ", st_blocks", ", st_atime", ", st_mtime",
                                 ", st_ctime", NULL };
            print_struct(prefixes, pid, uregs.rsi);
            break;
    }
    fprintf(stderr, ") = ");
}
