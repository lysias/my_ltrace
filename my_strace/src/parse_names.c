#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "error.h"

char** get_syscall_names() {
    FILE* fp = fopen("/usr/include/asm/unistd_64.h", "r");
    if (!fp)
        ERROR("unable to open /usr/include/asm/unistd_64.h");

    char* line = NULL;
    size_t len = 0;
    ssize_t read;

    size_t size  = 300; // The size of the array containing the syscalls
    size_t index = 0;
    char* sub    = NULL; // The index of the substring "__NR_" in the current line

    char** syscalls = calloc(size, sizeof(char*));
    if (!syscalls)
        ERROR("error while allocating space for syscalls");

    while ((read = getline(&line, &len, fp)) != -1) {
        // If the line contains a macro associating a syscall with a number...
        if ((sub = strstr(line, "__NR_"))) {
            // The name of the syscall is at the index of "__NR_" plus its size
            char* current = strtok(sub + 5, " ");

            // Realloc the array if we have more syscalls than expected
            if (index >= size) {
                size += size / 10;
                syscalls = realloc(syscalls, size * sizeof(char*));
                if (!syscalls)
                    ERROR("error while allocating space for syscalls");
            }
            syscalls[index++] = strdup(current);
        }
    }

    free(line);
    fclose(fp);

    return syscalls;
}

void free_tab(char** tab) {
    for (int i = 0; tab[i]; ++i)
        free(tab[i]);
    free(tab);
}
