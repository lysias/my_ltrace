#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>

#include "error.h"
#include "parse_names.h"
#include "printer.h"

void exec_child(char** argv) {
    ptrace(PTRACE_TRACEME);
    execvp(argv[0], argv);
}

int main(int argc, char** argv) {
    int status = 0;
    int pid    = 0;
    struct user_regs_struct uregs;

    if (argc < 2)
        ERROR("Argument missing\nUsage: ./my_strace <command>");

    if ((pid = fork()) == 0) {
        exec_child(argv + 1);
    }
    else {
        char** syscalls = get_syscall_names();

        wait(&status);
        ptrace(PTRACE_GETREGS, pid, 0, &uregs);

        // Print the first execve()
        print_syscall(syscalls, uregs, pid);
        fprintf(stderr, "%u\n", (unsigned int)uregs.rax);

        while (1) {
            ptrace(PTRACE_SYSCALL, pid, 0, 0); // Continue until next syscall
            wait(&status);
            ptrace(PTRACE_SYSCALL, pid, 0, 0); // Execute interrupted syscall
            wait(&status);
            ptrace(PTRACE_GETREGS, pid, 0, &uregs);
            print_syscall(syscalls, uregs, pid);

            if (WIFEXITED(status))
                break;

            // Print the return value of the syscall
            fprintf(stderr, "%llu\n", uregs.rax);
        }
        fprintf(stderr, "?\n+++ exited with %d +++\n", WEXITSTATUS(status));
        free_tab(syscalls);
    }

    return 0;
}

