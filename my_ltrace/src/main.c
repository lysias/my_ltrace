#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/wait.h>

#include "utils.h"
#include "readelf.h"
#include "breakpoint.h"

static char* get_file(char* filename) {
    char* path = getenv("PATH");
    size_t filename_len = strlen(filename);

    char* token = strtok(path, ":");

    // Concatenate the string given as argument to every path of the PATH
    // environment variable and look for the ELF file
    while(token != NULL) {
        size_t absolute_path_len = sizeof(char) * (filename_len + strlen(token));
        char absolute_path[absolute_path_len + 1];
        strncpy(absolute_path, token, strlen(token) + 1);
        strncat(absolute_path, "/", 1);
        strncat(absolute_path, filename, filename_len);

        // If the file exists and is readable, we won
        if (access(absolute_path, R_OK) != -1)
            return strndup(absolute_path, absolute_path_len + 1);
        token = strtok(NULL, ":");
    }
    return NULL;
}

void exec_child(char* path, char** argv) {
    ptrace(PTRACE_TRACEME);
    execvp(path, argv);
}

int main(int argc, char* argv[]) {
    int status = 0;
    int pid    = 0;

    if (argc < 2)
        ERROR("Argument missing\nUsage: ./my_ltrace <command>");

    char* path = access(argv[1], R_OK) != -1 ? strdup(argv[1]) :
                                               get_file(argv[1]);
    if (!path)
        ERROR("Could not find ELF file");

    s_list* symbols = read_elf(path);
    if (!symbols)
        ERROR("No symbols found");

    if ((pid = fork()) == 0) {
        exec_child(path, argv + 1);
    }
    else {
        wait(&status);

        s_list* breakpoints = insert_breakpoints(symbols, pid);

        while (1) {
            ptrace(PTRACE_CONT, pid, 0, 0);
            wait(&status);
            if (WIFEXITED(status))
                break;
            print_and_reset(breakpoints, pid);
        }
        fprintf(stderr, "+++ exited with %d +++\n", WEXITSTATUS(status));
        list_free(breakpoints, free);
    }
    list_free(symbols, free_symbol);
    free(path);

    return 0;
}
