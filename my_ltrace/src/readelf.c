#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "utils.h"
#include "readelf.h"

static Elf64_Shdr get_section(const char* name, char* elf) {
    Elf64_Ehdr* elf_header = (Elf64_Ehdr*)elf;

    // Section header table
    Elf64_Shdr* section_table = (Elf64_Shdr*)(elf + elf_header->e_shoff);

    // Number of entries in the section header table
    int count = elf_header->e_shnum;

    // Section header string table
    Elf64_Shdr* string_table = section_table + elf_header->e_shstrndx;
    const char* string_table_data = elf + string_table->sh_offset;

    for (int i = 0; i < count; ++i) {
        const char* current = string_table_data + section_table[i].sh_name;

        if (STREQ(current, name))
            return section_table[i];
    }
    return section_table[0];
}

static s_list* get_symbols(char* elf) {
    Elf64_Shdr plt      = get_section(".plt"      , elf);
    Elf64_Shdr rela_plt = get_section(".rela.plt" , elf);
    Elf64_Shdr dynsym   = get_section(".dynsym"   , elf);
    Elf64_Shdr dynstr   = get_section(".dynstr"   , elf);
    if (plt.sh_name    == 0 || rela_plt.sh_name == 0 ||
        dynsym.sh_name == 0 || dynstr.sh_name   == 0)
        return NULL;

    // Number of symbols in the .rela.plt section
    const size_t count = rela_plt.sh_size / rela_plt.sh_entsize;
    s_list* symbols = NULL;

    for (size_t i = 0; i < count; ++i) {
        // ith relocation
        Elf64_Rela* rel = (Elf64_Rela*)(elf + rela_plt.sh_offset +
                                        i * rela_plt.sh_entsize);
        // Index of the current relocation's symbol the the dynsym table
        int dynsym_index = ELF64_R_SYM(rel->r_info);
        Elf64_Sym* sym = (Elf64_Sym*)(elf + dynsym.sh_offset +
                                      dynsym_index * sizeof(Elf64_Sym));
        int dynstr_index = sym->st_name;
        char* name = (char*)elf + dynstr.sh_offset + dynstr_index;

        // Create new symbol
        s_symbol* symbol = calloc(1, sizeof(s_symbol));
        symbol->addr = plt.sh_addr + plt.sh_entsize + i * plt.sh_entsize;
        symbol->name = strdup(name);
        symbols = list_append(symbols, symbol);
    }
    return symbols;
}

s_list* read_elf(const char* filename) {
    struct stat st;

    if (stat(filename, &st) != 0)
        ERROR("Cannot get file status");

    int fd = open(filename, O_RDONLY);
    if (fd == -1)
        ERROR("Could not open file");

    char* elf = mmap(0, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (elf == MAP_FAILED) {
        ERROR("Failed to mmap file");
        close(fd);
    }

    s_list* symbols = get_symbols(elf);

    close(fd);
    munmap(elf, st.st_size);

    return symbols;
}

void free_symbol(void* ptr) {
    s_symbol* symbol = ptr;
    free(symbol->name);
    free(symbol);
}
