#include <stdlib.h>
#include <stdarg.h>
#include "list.h"

s_list* init_element(void* value, s_list* next) {
    s_list* new = malloc(sizeof(s_list));
    if (!new)
        return NULL;

    new->value = value;
    new->next = next;

    return new;
}

s_list* list_init(int nb_of_elts, void* first, ...) {
    s_list* new = init_element(first, NULL);
    s_list* head = new;

    va_list list;

    va_start(list, first);
    for (int i = 1; i < nb_of_elts; ++i) {
        new->next = init_element(va_arg(list, void*), NULL);
        new = new->next;
    }

    return head;
}

s_list* list_add(s_list* list, int index, void* value) {
    if (!list)
        return list_init(1, value);
    if (index == 0)
         return init_element(value, list);

    s_list* head = list;
    s_list* prev = list;
    for (int i = 0; i < index && list; ++i) {
        prev = list;
        list = list->next;
    }

    s_list* new = init_element(value, NULL);
    prev->next = new;
    new->next = list;

    return head;
}

s_list* list_append(s_list* list, void* value) {
    if (!list)
        return list_init(1, value);

    s_list* head = list;
    while (list->next)
        list = list->next;

    list->next = init_element(value, NULL);

    return head;
}

s_list* list_search(s_list* list, void* value, int cmp (void*, void*)) {
    s_list* tmp = list;
    while (tmp) {
        if (cmp(tmp->value, value))
            return tmp;
        tmp = tmp->next;
    }

    return NULL;
}

void list_free(s_list* list, void free_fun (void*)) {
    while (list) {
        s_list* tmp = list->next;
        free_fun(list->value);
        free(list);
        list = tmp;
    }
}
