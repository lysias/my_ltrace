#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>

#include "list.h"
#include "breakpoint.h"

s_list* insert_breakpoints(s_list* symbols, int pid) {
    s_list* breakpoints = NULL;

    while (symbols) {
        s_symbol* sym = symbols->value;

        // Save the data originaly present at the address where we put the
        // breakpoint
        long saved = ptrace(PTRACE_PEEKDATA, pid, sym->addr);

        // Initialize the new breakpoint and append it to the list
        s_breakpoint* breakpoint = calloc(1, sizeof(s_breakpoint));
        breakpoint->symbol = sym;
        breakpoint->data = saved;
        breakpoints = list_append(breakpoints, breakpoint);

        // Insert the breakpoint
        long opcode = (saved & 0xFFFFFFFFFFFFFF00) | 0xCC;
        ptrace(PTRACE_POKEDATA, pid, sym->addr, opcode);
        symbols = symbols->next;
    }

    return breakpoints;
}

static int cmp_function(void* bp1, void* bp2) {
    return ((s_breakpoint*)bp1)->symbol->addr == (unsigned long)bp2;
}

void print_and_reset(s_list* breakpoints, int pid) {
    struct user_regs_struct uregs;
    ptrace(PTRACE_GETREGS, pid, 0, &uregs);
    uregs.rip -= 1; // Because the breakpoint instruction was executed
    ptrace(PTRACE_SETREGS, pid, 0, &uregs);

    // Get the current breakpoint...
    s_list* tmp = list_search(breakpoints, (void*)uregs.rip , cmp_function);

    if (tmp) {
        // ... print the name of the function called...
        s_breakpoint* breakpoint = tmp->value;
        fprintf(stderr, "%s() %*s %lld\n", breakpoint->symbol->name,
                40 - (int)strlen(breakpoint->symbol->name), "=",uregs.rax);

        // ... then execute and reset the breakpoint
        ptrace(PTRACE_POKEDATA, pid, breakpoint->symbol->addr, breakpoint->data);
        ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
        wait(NULL);

        ptrace(PTRACE_GETREGS, pid, 0, &uregs);
        long opcode = (breakpoint->data & 0xFFFFFFFFFFFFFF00) | 0xCC;
        ptrace(PTRACE_POKEDATA, pid, breakpoint->symbol->addr, opcode);
    }
}
