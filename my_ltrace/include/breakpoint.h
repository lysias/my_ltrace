#ifndef BREAKPOINT_H
# define BREAKPOINT_H

# include "readelf.h"

typedef struct {
    s_symbol* symbol;
    long data; ///< data at addr (saved when inserting the breakpoint
} s_breakpoint;

/**
** @brief Creates and initializes the breakpoints
**
** @param symbols The symbols on which to break
** @param pid The pid of the tracee
**
** @return The newly created list of breakpoints
*/
s_list* insert_breakpoints(s_list* symbols, int pid);

/**
** @brief Prints the name of the function called and resets the breakpoint for
** the next call
**
** @param breakpoints The list of breakpoints
** @param pid The pid of the tracee
*/
void print_and_reset(s_list* breakpoints, int pid);

#endif /* !BREAKPOINT_H */

