#ifndef READELF_H
# define READELF_H

# include "list.h"
# include <elf.h>

typedef struct {
    char* name;
    Elf64_Addr addr; ///< address of the .plt section + position * 16 + 16
} s_symbol;

/**
** @brief Opens an elf file, performs checks and gets the symbols
**
** This function retrieves the names of all the external functions possibly
** called by the elf along with their offsets in the PLT (that are used for the
** breakpoints)
**
** @param filename The path of the elf file to open
**
** @return A list of s_symbol
*/
s_list* read_elf(const char* filename);
void free_symbol(void* ptr);

#endif /* !READELF_H */

