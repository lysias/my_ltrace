#ifndef UTILS_H
# define UTILS_H

# define ERROR(message) {                                  \
    printf("%s::%s(): %s\n", __FILE__, __func__, message); \
    exit(1);                                               \
}

# define STREQ(a,b) \
    (strcmp ((a), (b)) == 0)

#endif /* !UTILS_H */
