#ifndef LIST_H
# define LIST_H

typedef struct list s_list;

struct list {
    void* value;
    s_list *next;
};

/**
** @brief The comparison function used for searching in the list
*/
int (*cmp_fun) (void*, void*);

void (*free_fun) (void*);

s_list* init_element(void* value, s_list* next);

/**
** @brief Inits and returns a new generic list
**
** @param nb_of_args Number of elements to add to the list
** @param first First element to add to the list
** @param ...
**
** Since there is no way to determine the number of variable arguments
** passed to the function, I had to choose between hoping people would
** rememeber to put a NULL pointer at the end of the arguments or forcing them
** to provide the number of arguments they wanted to add.
**
** @return The newly created list
*/
s_list* list_init(int nb_of_elts, void* first, ...);

/**
** @brief Inserts value at the indicated index in list
**
** @param list The list in which to insert the element
** @param index The index at which to insert the element
** @param value The value to insert
**
** @return The new list
*/
s_list* list_add(s_list* list, int index, void* value);
s_list* list_append(s_list* list, void* value);

/**
** @brief Searches and returns, if found, the node containing value
**
** @param list The list in which to search
** @param value The value to look for
**
** @return The node containing the value if found. NULL otherwise
*/
s_list* list_search(s_list* list, void* value, int cmp (void*, void*));

void list_free(s_list* list, void free (void*));

#endif /* !LIST_H */
